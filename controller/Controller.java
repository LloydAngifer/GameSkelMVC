/*
 * Copyright (C) 2016 Virgil Manrique
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gameskelmvc.controller;

import gameskelmvc.model.Model;
import gameskelmvc.view.View;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class for managing user interactions
 * 
 * @author Virgil Manrique
 */
public class Controller implements KeyListener, ActionListener, MouseListener
{
	private final View view;
	private final Model model;
	
    /**
     * Controller's Constructor
     * @param view        View to manage
     * @param mod         Model to manage
     */
    public Controller(View view, Model mod)
	{
		this.view = view;
		this.model = mod;
	}

    /**
     * Method for performing an action depending on the key pressed by the user
     * @param e     event corresponding to the key pressed
     */
    @Override
	public void keyPressed(KeyEvent e)
	{
		int key = e.getKeyCode();
		if (key == KeyEvent.VK_ESCAPE)
		{
            stop("The Esc key was pressed, stop in progress...");	
		}
	}

    /**
     * Method for performing an action depending on the key released by the user
     * @param e     event corresponding to the key released
     */
    @Override
	public void keyReleased(KeyEvent e)
	{
        throw new UnsupportedOperationException("Not supported yet.");
	}

    /**
     * Method for performing an action depending on the key typed by the user
     * @param e     event corresponding to the key released
     */
    @Override
	public void keyTyped(KeyEvent e)
	{
        throw new UnsupportedOperationException("Not supported yet.");
	}

    /**
     * Called when a button is clicked by the user
     * @param e     event corresponding to the button
     */
    @Override
	public void actionPerformed(ActionEvent e)
	{
		Object source = e.getSource();
		int index = view.getIndexMenuItem(source);
		
		switch(index)
		{
			case 0 :
				stop("The \"Exit\" button has been clicked, stop in progress...");
				break;
			case 1 : 
				System.out.println("Save Button");
                {
                    try
                    {
                        model.save();
                    }
                    catch(URISyntaxException ex)
                    {
                        Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
				break;
			case 2 :
				System.out.println("Load Button");
                {
                    try
                    {
                        model.load();
                    }
                    catch(URISyntaxException ex)
                    {
                        Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
				break;
			default:
				System.out.println("????");
				break;
		}	
	}
    
    private void stop(String cause)
    {
    	System.out.println(cause);
		this.view.stop();
		this.view.close();	
    }

    /**
     * Method for performing an action when the mouse button is clicked.
     * @param me    MouseEvent
     */
    @Override
    public void mouseClicked(MouseEvent me)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Method for performing an action when the mouse button is pressed.
     * @param me    MouseEvent
     */
    @Override
    public void mousePressed(MouseEvent me)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Method for performing an action when the mouse button is released.
     * @param me    MouseEvent
     */
    @Override
    public void mouseReleased(MouseEvent me)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Method for performing an action when the mouse enter the element.
     * @param me    MouseEvent
     */
    @Override
    public void mouseEntered(MouseEvent me)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Method for performing an action when the mouse exit the element.
     * @param me    MouseEvent
     */
    @Override
    public void mouseExited(MouseEvent me)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
