/*
 * Copyright (C) 2016 Virgil Manrique
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gameskelmvc.view;

import gameskelmvc.model.Model;
import gameskelmvc.controller.Controller;
import java.awt.event.KeyEvent;

import javax.swing.*;

/**
 * Class for managing the display
 * 
 * @author Virgil Manrique
 */
public class View extends JFrame
{
	private static final long serialVersionUID = 1L;
	//private final Panel panel;
	private final Controller controller;
	private boolean finished;

	private final JMenuBar menuBar;

	private final JMenu menuFile;
	private final JMenuItem menuItemExit;
	private final JMenuItem menuItemSave;
	private final JMenuItem menuItemLoad;

	public View(Model model)
	{
		this.finished = false;
		this.controller = new Controller(this,model);

		this.setTitle("GameSkelMVC");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(1280, 720);
		this.setLocationRelativeTo(null);
		this.setResizable(false);

		this.setFocusable(true);
		this.addKeyListener(controller);
        //TODO: vérifier si il faut absolument le jpanel ou pas
		//this.panel = new Panel();
		//this.setContentPane(panel);
		this.setVisible(true);

		// Menu Bar
		this.menuBar = new JMenuBar();
		this.menuFile = new JMenu("File");
		this.menuFile.setMnemonic(KeyEvent.VK_F);

		this.menuItemSave = new JMenuItem("Save");
		this.menuItemSave.addActionListener(controller);
		this.menuFile.add(menuItemSave);

		this.menuItemLoad = new JMenuItem("Load");
		this.menuItemLoad.addActionListener(controller);
		this.menuFile.add(menuItemLoad);

		this.menuFile.addSeparator();

		this.menuItemExit = new JMenuItem("Exit");
		this.menuItemExit.addActionListener(controller);
		this.menuFile.add(menuItemExit);

		this.menuBar.add(menuFile);

		this.setJMenuBar(menuBar);
	}

    /**
     * Method used for refresh the view
     */
    public void render()
	{
		//this.panel.repaint();
	}

	/**
	 * Method to know if the view has been closed or not
	 * 
	 * @return value of the finished attribute
	 */
	public boolean hasClosed()
	{
		return this.finished;
	}

    /**
     * Method used to set the attributed finished to true
     */
    public void stop()
	{
		this.finished = true;
	}

    /**
     * Method used to close the view
     */
    public void close()
	{
		this.setVisible(false);
		this.dispose();
	}

    /**
     * Method that returns an integer corresponding to the index of the parameter MenuItem
     * @param menuItem  object that we want the index
     * @return          index of menuItem or -1 if the object is not one
     */
    public int getIndexMenuItem(Object menuItem)
	{
		if(menuItem == this.menuItemExit)
        {
            return 0;
        }
		if(menuItem == this.menuItemSave)
        {
            return 1;
        }
		if(menuItem == this.menuItemLoad)
        {
            return 2;
        }
			
		return -1;
	}
}
