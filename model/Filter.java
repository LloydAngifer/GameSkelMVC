/*
 * Copyright (C) 2016 Virgil Manrique
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gameskelmvc.model;

import java.io.File;
import javax.swing.filechooser.FileFilter;

/**
 * Class used to add a filter to a FileChooser
 * 
 * @author Virgil Manrique
 */
public class Filter extends FileFilter
{
    String suffix[];
    String description;
    
    /**
     * Filter's Constructor
     * @param suf      suffixes to filter
     * @param description   filter's description
     */
    public Filter(String []suf, String description)
    {
        this.suffix = suf;
        this.description = description;
    }
    
    boolean belongTo(String suf)
    {
        for( int i = 0; i<this.suffix.length; ++i)
        {
            if(suf.equals(this.suffix[i]))
            {
                return true; 
            }
        }
        return false;
    }

    /**
     * Function used to check if the file pass the filter or not
     * @param f     file whose name need to be verified
     * @return      true if the file is visible or false if not
     */
    @Override
    public boolean accept(File f) {
        if (f.isDirectory())
        {
            return true;
        }
        String suf = null;
        String fileName = f.getName();
        int i = fileName.lastIndexOf('.');
        if((i>0) && (i<fileName.length()-1))
        {
            suf=fileName.substring(i+1).toLowerCase();
        }
        return suf!=null&&belongTo(suf);
    }

    /**
     * Function to get the filter's description
     * @return attribute description
     */
    @Override
    public String getDescription() {
        return description;
    }
}

