/*
 * Copyright (C) 2016 Virgil Manrique
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gameskelmvc.model;

import gameskelmvc.view.View;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URISyntaxException;
import java.net.URL;
import javax.swing.JFileChooser;

/**
 * Class for managing the running of the application
 * 
 * @author Virgil Manrique
 */
public class Model
{

	private final View view;
    private final JFileChooser choice;
    private final Filter fdat;

	/**
	 * Model's Contructor
	 */
	public Model()
	{ 
        this.choice = new JFileChooser();
        this.fdat = new Filter(new String[]{"dat"},"Save files (*.dat)");
        choice.addChoosableFileFilter(fdat);
        choice.setAcceptAllFileFilterUsed(false);
        
        this.view=new View(this);
	}

    /**
     * Method used to check if a file exist
     * @param path      path of the file
     * @return          true if the file exist, false if not
     */
    public boolean checkIfFileExist(String path)
	{
		File fichier = new File(path);
		return fichier.exists();
	}

    /**
     * Method for the main loop of the game
     */
    public void run()
	{
		while (!view.hasClosed())
			{
				view.render();
				try //TODO comprendre pourquoi il faut faire un sleep
				{
                    Thread.sleep(5);
				}
                catch (Exception e)
				{
				}
			}
		System.out.println("The game stopped successfully!");
	}

    /**
     * Method used to save the game
     * @throws URISyntaxException exception throw if there is a problem with the game path
     */
    public void save() throws URISyntaxException
	{
        URL u = getClass().getProtectionDomain().getCodeSource().getLocation();
        File f = new File(u.toURI());
        this.choice.setCurrentDirectory(f.getParentFile());
        int choiceReturn;
        choiceReturn = this.choice.showSaveDialog(view);
        if(choiceReturn==JFileChooser.APPROVE_OPTION) // if a file is choosed
        {
            // absolute path of the file
            String path;
            path = choice.getSelectedFile().getAbsolutePath();
            // add a .dat extension if the file don't have this extension
            if(!path.endsWith(".dat"))
            {
               path=path+".dat";
            }
            try
            {
               FileOutputStream fos = new FileOutputStream(path);
               ObjectOutputStream oos = new ObjectOutputStream (fos);
               /*
               oos.writeObject(); //Put here what you want to save
               oos.flush();
               */
               fos.close();
               System.out.println("Save finished: "+path);
            }
            catch(Exception e)
            {
                System.out.println("Error:"+e.getMessage());
            }
        }
        else // if the user does'nt choose a file
        {
            System.out.println("No file choosed by the user");
        }
	}

    /**
     * Method used to load a previous game
     * @throws URISyntaxException URISyntaxException exception throw if there is a problem with the game path
     */
    public void load() throws URISyntaxException
	{
        URL u = getClass().getProtectionDomain().getCodeSource().getLocation();
        File f = new File(u.toURI());
        choice.setCurrentDirectory(f.getParentFile());
        int choiceReturn;
        choiceReturn = choice.showOpenDialog(view);
        if(choiceReturn==JFileChooser.APPROVE_OPTION) // if a file is choose
        {
            // absolute path of the choosed file
            String path;
            path = choice.getSelectedFile().getAbsolutePath();
            try
            {  
                FileInputStream file=new FileInputStream(path);
                ObjectInputStream ois=new ObjectInputStream (file);
                while (file.available()>0)
                {
                    //put here what you want to load
                    ois.readObject();
                }
                file.close();
                System.out.println("Load finished: "+path);
            }
            catch(IOException | ClassNotFoundException e)
            {
                System.out.println("Error:"+e.getMessage());
            }
        }
        else // if the user doesn't choose a file
        {
            System.out.println("No file choosed by the user");
        }
	}
}
