/*
 * Copyright (C) 2016 Virgil Manrique
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package gameskelmvc.model;

/**
 * Class for managing a position in a 2-dimensional space
 * 
 * @author Virgil Manrique
 */
public class Position
{
	private double x;
    private double y;

    /**
     * Constructor of a position with the coordinates given.
     * @param x Coordinate on X axis
     * @param y Coordinate on Y axis
     */
    public Position(double x, double y)
    {
		this.x = x;
		this.y = y;
    }

    /**
     * Constructor of a position without parameters, set the position to the origin.
     */
    public Position()
    {
        this(0,0);
    }

    /**
     * Sets the value of the attribute x.
     * @param x new value for the attribute x
     */
    public void setX(double x)
    {
        this.x = x;
    }
    
    /**
     * Sets the value of the attribute y.
     * @param y new value for the attribute y
     */
    public void setY(double y)
    {
        this.y = y;
    } 
    
    /**
     * Provides the value of the attribute x.
     * @return the x value of the position
     */
    public double getX()
    {
        return x;
    }

    /**
     * Provides the value of the attribute y.
     * @return the value y of the position
     */
    public double getY()
    {
        return y;
    }
    
    /**
     * Add a position to the actual position
     * @param pos   position to add
     */
    public void addTo(Position pos)
    {
        this.x=this.x+pos.getX();
        this.y=this.y+pos.getY();
    }
    
    /**
     * Return the Manhattan distance to the position given in parameter
     * @param pos   position from which calculate the distance
     * @return      distance to the position
     */
    public double ManhattanDistanceTo(Position pos)
    {
       return (Math.abs(pos.getX()-this.x)+Math.abs(pos.getY()-this.y));
    }
    
    /**
     * Return the Euclidian distance to the position given in parameter
     * @param pos   position from which calculate the distance
     * @return      distance to the position
     */
    public double EuclidianDistanceTo(Position pos)
    {
        return Math.sqrt(SquaredEuclidianDistanceTo(pos));
    }
    
    /**
     * Return the square of the Euclidian distance to the position given in parameter
     * @param pos   position from which calculate the distance
     * @return      square of the distance to the position
     */
    public double SquaredEuclidianDistanceTo(Position pos)
    {
        return (Math.pow(pos.getX()-this.x, 2)+Math.pow(pos.getY()-this.y,2));
    }
}
